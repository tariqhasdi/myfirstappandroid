package com.example.applist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import com.example.applist.model.Student;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<Student> list;

    public RecyclerViewAdapter(List<Student> list) {
        this.list = list;
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.student_layout, parent, false);
        return new RecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Student student = list.get(position);

        holder.studentPrenom.setText(student.getPrenom());
        holder.studentNom.setText(student.getNom());
        holder.studentImage.setImageResource(R.drawable.ic_launcher_foreground);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        final TextView studentPrenom;
        final TextView studentNom;
        final ImageView studentImage;

        ViewHolder(View view) {
            super(view);
            studentPrenom = view.findViewById(R.id.student_prenom);
            studentNom = view.findViewById(R.id.student_nom);
            studentImage = view.findViewById(R.id.student_image);
        }
    }
}
