package com.example.applist.model;

public class Student {

    private String prenom;
    private String nom;
    private String photo;

    public Student() {
        super();
    }

    public Student(String prenom, String nom, String photo) {
        super();
        this.prenom = prenom;
        this.nom = nom;
        this.photo = photo;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "Student{" +
                "prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", photo='" + photo + '\'' +
                '}';
    }
}
