package com.example.applist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import com.example.applist.model.Student;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        List<Student> list = new ArrayList<>();
        Student student = new Student("tariq", "hasdi", "vide");
        list.add(student);
        student = new Student("rayan", "hasdi", "vide");
        list.add(student);
        student = new Student("sophie", "bepoix", "vide");
        list.add(student);

        student = new Student("remy", "bépoix", "vide");
        list.add(student);
        student = new Student("serge", "bépoix", "vide");
        list.add(student);
        student = new Student("martine", "bepoix", "vide");
        list.add(student);

        student = new Student("tariq", "hasdi", "vide");
        list.add(student);
        student = new Student("rayan", "hasdi", "vide");
        list.add(student);
        student = new Student("sophie", "bepoix", "vide");
        list.add(student);

        student = new Student("remy", "bépoix", "vide");
        list.add(student);
        student = new Student("serge", "bépoix", "vide");
        list.add(student);
        student = new Student("martine", "bepoix", "vide");
        list.add(student);

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(list);
        recyclerView.setAdapter(adapter);

        setContentView(R.layout.student_layout);
    }
}
